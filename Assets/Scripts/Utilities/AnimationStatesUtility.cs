﻿using UnityEngine;

//TODO: Generate this dynamically through an editor script so we dont have to add new states here.
public static class AnimationStatesUtility
{
    public enum AnimationState
    {
        Push,
        Attack01,
        Attack02,
        Victory01,
        Dance01,
        Run01FWD,
        Run01BWD,
        Run01Left,
        Run01Right,
        JumpOneTake,
        Slide,
        Defense,
        DefenseHit,
        GetHit,
        Dizzy,
        Die01
    }

    static string _defaultState = "RunBlendTree";
    public static string DefaultState => _defaultState;

    public static int GetAnimationStateId(AnimationState state)
    {
        switch (state)
        {
            case AnimationState.Push: return Animator.StringToHash("Push");
            case AnimationState.Attack01: return Animator.StringToHash("Attack01");
            case AnimationState.Attack02: return Animator.StringToHash("Attack02");
            case AnimationState.Victory01: return Animator.StringToHash("Victory01");
            case AnimationState.Dance01: return Animator.StringToHash("Dance01");
            case AnimationState.Run01FWD: return Animator.StringToHash("Run01FWD");
            case AnimationState.Run01BWD: return Animator.StringToHash("Run01BWD");
            case AnimationState.Run01Left: return Animator.StringToHash("Run01Left");
            case AnimationState.Run01Right: return Animator.StringToHash("Run01Right");
            case AnimationState.JumpOneTake: return Animator.StringToHash("JumpOneTake");
            case AnimationState.Slide: return Animator.StringToHash("Slide");
            case AnimationState.Defense: return Animator.StringToHash("Defense");
            case AnimationState.DefenseHit: return Animator.StringToHash("DefenseHit");
            case AnimationState.GetHit: return Animator.StringToHash("GetHit");
            case AnimationState.Dizzy: return Animator.StringToHash("Dizzy");
            case AnimationState.Die01: return Animator.StringToHash("Die01");
        }
        return -1;
    }
}