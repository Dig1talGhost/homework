﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] MovementEvent _movementEvent;
    NavMeshAgent _agent;
    Animator _animator;
    bool _isPlayingAnimation = false;


    void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>(); 
    }
    
    void OnEnable()
    {
        _movementEvent.OnRaise += Move;
    }

    void OnDisable()
    {
        _movementEvent.OnRaise -= Move;
    }

    // Update is called once per frame
    void Move(Vector3 position)
    {
        if (!_isPlayingAnimation)
        {
            _agent.SetDestination(position);
        }
    }

    void Update()
    {
        _animator.SetFloat("ForwardAxisMovement", Vector3.Dot(transform.forward, _agent.destination - transform.position));
        _animator.SetFloat("SideAxisMovement", Vector3.Dot(transform.right, _agent.destination - transform.position));
    }

    public void PlayAnimation(AnimationStatesUtility.AnimationState animationState)
    {
        StartCoroutine(WaitForAnimation(animationState));
    }

    IEnumerator WaitForAnimation(AnimationStatesUtility.AnimationState animationState)
    {
        _agent.SetDestination(transform.position);
        _agent.isStopped = true;
        _isPlayingAnimation = true;

        _animator.Play(AnimationStatesUtility.GetAnimationStateId(animationState));

        //Give the animator one frame to start playing the new animation
        yield return null;

        //Wait until we return to the default animation state before we can move again
        while (!_animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStatesUtility.DefaultState))
        {
            yield return null;
        }

        _agent.isStopped = false;
        _isPlayingAnimation = false;
    }
}
