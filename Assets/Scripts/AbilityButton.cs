﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AbilityButton : MonoBehaviour
{
    Button _button;
    [SerializeField] AnimationStatesUtility.AnimationState _animationState;
    PlayerAbilityPanel _panel;
    
    void Awake()
    {
        _button = GetComponent<Button>();
        _panel = GetComponentInParent<PlayerAbilityPanel>();
        if (_panel == null)
        {
            throw new System.Exception("Ability button not a child of PlayerAbilityPanelObject.");
        }
    }

    void OnEnable()
    {
        _button.onClick.AddListener(PlayAnimation);
    }

    void PlayAnimation()
    {
        _panel.PlayAnimation(_animationState);
    }
}
