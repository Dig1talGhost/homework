﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericGameEvent<T> : ScriptableObject
{
    public event System.Action<T> OnRaise;
    public void Raise(T arg)
    {
        OnRaise?.Invoke(arg);
    }
}
