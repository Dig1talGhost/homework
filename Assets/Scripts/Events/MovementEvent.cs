﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewMovementEvent", menuName = "ScriptableObjects/Movement Event", order = 1)]
public class MovementEvent : GenericGameEvent<Vector3> { }
