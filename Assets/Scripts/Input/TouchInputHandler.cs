﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInputHandler : MonoBehaviour, IInputHandler
{
    public bool GetPointerDown()
    {
        foreach (Touch t in Input.touches)
        {
            if (t.phase != TouchPhase.Ended && t.phase != TouchPhase.Canceled)
            {
                //Then we have a touch!
                return true;
            }
        }
        return false;
    }

    public Vector2 GetPointerScreenPosition()
    {
        if (Input.touchCount > 0)
        {
            return Input.touches[Input.touches.Length].position;
        }
        else
        {
            return Vector2.zero;
        }
    }

    public bool GetPointerUp()
    {
        bool lostATouch = false;
        foreach (Touch t in Input.touches)
        {
            if (t.phase == TouchPhase.Ended && t.phase == TouchPhase.Canceled)
            {
                //Then we have lost a touch!
                lostATouch = true;
            }
        }

        return lostATouch;
    }
}