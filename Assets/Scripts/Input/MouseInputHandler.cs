﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MouseInputHandler : MonoBehaviour, IInputHandler
{
    public bool GetPointerDown()
    {
        return Input.GetMouseButtonDown(0);
    }
    public Vector2 GetPointerScreenPosition()
    {
        return Input.mousePosition;
    }
    public bool GetPointerUp()
    {
        return Input.GetMouseButtonUp(0);
    }
}