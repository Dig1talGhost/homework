﻿using UnityEngine;

public interface IInputHandler
{
    bool GetPointerDown();
    bool GetPointerUp();
    Vector2 GetPointerScreenPosition();
}
