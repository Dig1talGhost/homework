﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputRaycaster : MonoBehaviour
{
    [SerializeField] Camera _originCamera = null;
    [SerializeField] MovementEvent _movementEvent = null;
    [SerializeField] LayerMask _raycastLayerMask = default;
    [SerializeField] GraphicRaycaster _blockingRaycaster = null;

    IInputHandler[] _inputHandlers;

    void Awake()
    {
        //Get all the input handlers
        _inputHandlers = GetComponents<IInputHandler>();
    }

    void Update()
    {
        foreach (IInputHandler handler in _inputHandlers)
        {
            if (handler.GetPointerDown())
            {
                if (CheckAgainstUI(handler.GetPointerScreenPosition()))
                {
                    continue;
                }

                Ray ray = _originCamera.ScreenPointToRay(handler.GetPointerScreenPosition());
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100, _raycastLayerMask))
                {
                    Debug.DrawLine(ray.origin, hit.point);
                    _movementEvent.Raise(hit.point);
                }
            }
        }
    }

    //Checks to see if we have raycast into any UI objects. If so, returns true.
    bool CheckAgainstUI(Vector2 screenPoint)
    {
        var pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = screenPoint;

        List<RaycastResult> results = new List<RaycastResult>();

        _blockingRaycaster.Raycast(pointerEventData, results);

        return results.Count > 0;
    }
}
