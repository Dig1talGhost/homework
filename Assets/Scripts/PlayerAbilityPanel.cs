﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbilityPanel : MonoBehaviour
{
    [SerializeField] PlayerController _player;

    public void PlayAnimation(AnimationStatesUtility.AnimationState animationState)
    {
        _player.PlayAnimation(animationState);
    }
}
